#!/bin/sh

set -e

TMPFILE=/usr/lib/tmpfiles.d/mildenhall-extensions.conf
SOURCE=/var/lib/
TARGET=/usr/share/
MILDENHALL=MILDENHALL_extensions

# Move the MILDENHALL_extensions directory to /usr/share
if [ -d $SOURCE/$MILDENHALL ] ; then
	mkdir -p $TARGET
	mv $SOURCE/$MILDENHALL $TARGET/

	# Create a symbolic link at the standard place
	echo "L+ $SOURCE/$MILDENHALL - - - - $TARGET/$MILDENHALL" > $TMPFILE
fi
