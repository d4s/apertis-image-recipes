{{- $architecture := or .architecture "amd64" }}
{{- $type := or .type "devroot" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "18.12" -}}
{{- $timestamp := or .timestamp "00000000.0" -}}
{{- $ospack := or .ospack (printf "ospack_%s-%s-%s_%s" $suite $architecture $type $timestamp) -}}
{{- $ivitools := or .ivitools "enabled" -}}
{{- $lxc := or .lxc "enabled" -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{ $suite }}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: apertis-archive-keyring
    keyring-file: keyring/apertis-archive-keyring.gpg
    merged-usr: false

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh apertis {{ $suite }} {{ $timestamp }} collabora {{ $type }}

  # Extend apt sources list
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} target

  {{ if eq $ivitools "enabled" }}
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} hmi helper-libs
  {{ end }}

  # Add type-based apt sources list
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} development

  - action: run
    chroot: true
    script: scripts/replace-tar-coreutils-for-build.sh

  - action: apt
    description: "Core packages"
    packages:
      - sudo
      - apt-transport-https
      - apertis-customizations
      - initramfs-tools

  - action: apt
    description: "Base packages"
    packages:
      - busybox
      - busybox-initramfs
      - dbus-user-session

  - action: apt
    description: "Networking packages"
    packages:
      - connman
      - wpasupplicant

  - action: apt
    description: "AppArmor packages"
    packages:
      - apparmor
      - chaiwala-apparmor-profiles

  {{ if eq $ivitools "enabled" }}
  - action: apt
    description: "Application framework packages"
    packages:
      - canterbury-core
      - newport
      - ribchester-core
  {{ end }}

  - action: apt
    description: "Test environment packages"
    packages:
      - net-tools
      - openssh-client
      - openssh-server
      - vim.tiny

  {{ if eq $ivitools "enabled" }}
  - action: apt
    description: "HMI packages"
    packages:
      - eye
      - frampton
      - mildenhall
      - mildenhall-compositor
      - mildenhall-launcher
      - mildenhall-popup-layer
      - mildenhall-settings
      - mildenhall-statusbar
      - xwayland
  {{ end }}

  - action: apt
    description: "Target user session packages"
    packages:
      - chaiwala-user-session-wayland

  - action: apt
    description: "Target packages"
    packages:
      - adduser
      - apertis-archive-keyring
      - apertis-customizations
      - apparmor
      - apparmor-profiles
      - apt
      - apt-transport-https
      - apt-utils
      - auditd
      - avahi-daemon
      - bluez
      - bluez-obexd
      - btrfs-tools
      - busybox-initramfs
      - bzip2
      - ca-certificates
      - chaiwala-apparmor-profiles
      - connman
      - debconf-i18n
      - evolution-data-server
      - file
      - fonts-dejavu-extra
      - gnupg
      - gstreamer1.0-clutter-3.0
      - gstreamer1.0-plugins-good
      - gstreamer1.0-pulseaudio
      - gvfs
      - initramfs-tools
      - iproute
      - iptables
      - kmod
      - libgupnp-1.0-4
      - liblockfile-bin
      - libnss-myhostname
      - libproxy1-pacrunner
      - libwebkit2gtk-4.0-37
      - linux-firmware
      - locales
      - lsb-base
      - lzma
      - mawk
      - mobile-broadband-provider-info
      - mutter
      - net-tools
      - netbase
      - openssh-client
      - openssh-server
      - pacrunner
      - plymouth
      - plymouth-themes
      - policykit-1
      - pulseaudio
      - pulseaudio-module-bluetooth
      - pulseaudio-module-x11
      - pulseaudio-utils
      - sudo
      - systemd-sysv
      - tcmmd
      - tracker
      - tracker-miner-fs
      - tumbler
      - tumbler-plugins-extra
      - udev
      - usb-modeswitch
      - vim-tiny
      - whiptail
      - wpasupplicant
      - xauth
      - xdg-user-dirs
      - xwayland

  {{ if eq $ivitools "enabled" }}
  - action: apt
    description: "Target packages (CM)"
    packages:
      - barkway
      - beckfoot
      - canterbury
      - didcot
      - dlt-daemon
      - frome
      - geoclue
      - grilo-plugins-0.2
      - newport
      - ofono
      - prestwood
      - rhosydd
      - ribchester
      - shapwick
      - syncevolution
      - tinwell
  {{ end }}

  {{ if eq $lxc "enabled" }}
  - action: apt
    description: "LXC packages"
    packages:
      - libpam-cgfs
      - lxc
      - lxc-templates
      - uidmap

  - action: overlay
    description: "Install the Apertis template to LXC"
    source: lxc/lxc-apertis-ostree
    destination: /usr/share/lxc/templates/lxc-apertis-ostree

  - action: run
    description: "Set executable bit on Apertis LXC template"
    chroot: true
    command: chmod a+x /usr/share/lxc/templates/lxc-apertis-ostree
  {{ end }}

  {{ if eq $ivitools "enabled" }}
  - action: apt
    description: "Development HMI packages"
    packages:
      - mildenhall-dev
  {{ end }}

  - action: apt
    description: "Development packages"
    packages:
      - apparmor-utils
      - automake
      - autopoint
      - autotools-dev
      - bash
      - bison
      - build-essential
      - bustle-pcap
      - chrpath
      - clutter-1.0-tests
      - cmake
      - cmake-data
      - connman-tests
      - d-feet
      - debhelper
      - dosfstools
      - evolution-data-server-dbg
      - evolution-data-server-dev
      - fakeroot
      - flex
      - gawk
      - gcc
      - gdb
      - gdbserver
      - gir1.2-secret-1
      - git
      - gperf
      - gstreamer1.0-tools
      - gtk-doc-tools
      - iputils-ping
      - less
      - libasound2-dev
      - libbluetooth-dev
      - libclutter-1.0-dbg
      - libclutter-1.0-dev
      - libclutter-gst-3.0-dev
      - libclutter-gtk-1.0-dev
      - libcogl-dev
      - libcurl4-nss-dev
      - libenchant-dev
      - libffi-dev
      - libgirepository1.0-dev
      - libglib2.0-0-dbg
      - libglib2.0-dev
      - libgnomevfs2-dev
      - libgstreamer-plugins-base1.0-dev
      - libgstreamer1.0-dev
      - libgtk2.0-dev
      - libgtk-3-0-dbg
      - libgudev-1.0-dev
      - libgupnp-1.0-dev
      - libhyphen-dev
      - libicu-dev
      - libjpeg-dev
      - libjsoncpp1
      - libmutter-dev
      - libmx-2.0-0-dev
      - libnotify-dev
      - libpango1.0-dev
      - libpng-dev
      - libpoppler-glib-dev
      - libproxy-dev
      - libpulse-dev
      - libsecret-1-0
      - libsecret-1-dev
      - libsecret-common
      - libsoup2.4-dev
      - libsqlite3-dev
      - libtool
      - libtracker-control-1.0-dev
      - libtracker-miner-1.0-dev
      - libtracker-sparql-1.0-dev
      - libwebkit2gtk-4.0-dev
      - libwebp-dev
      - libwnck-3-dev
      - libxslt1-dev
      - libxt-dev
  {{ if eq $architecture "armhf" }}
      - linux-headers-armmp
  {{ else }}
      - linux-headers-{{$architecture}}
  {{ end }}
      - lsb-release
      - ltrace
      - openssh-client
      - openssh-server
      - pavucontrol
      - pkg-config
      - python-pkg-resources
      - python-ply
      - ruby
      - slimit
      - strace
      - symlinks
      - syncevolution-dbg
      - valgrind
      - wget
      - xinput

  {{ if eq $ivitools "enabled" }}
  - action: apt
    description: "Development packages (CM)"
    packages:
      - barkway-dev
      - beckfoot-dev
      - canterbury-dev
      - folks-tools
      - frome
      - geoclue-2.0
      - libchamplain-0.12-dev
      - libclapton-dev
      - libdidcot-0-dev
      - libfolks-dbg
      - libfolks-dev
      - libfolks-eds-dbg
      - libfolks-eds-dev
      - libfolks-telepathy-dbg
      - libfolks-telepathy-dev
      - libfrome-0-dev
      - libgeoclue-dev
      - libgrassmoor-dev
      - libgupnp-av-1.0-dev
      - liblightwood-dev
      - libmrss0-dev
      - libseaton-dev
      - libtelepathy-farstream-dev
      - libtelepathy-glib-dev
      - libtelepathy-glib0-dbg
      - libthornbury-dev
      - libwebsockets-dev
      - newport-dev
      - prestwood-dev
      - ribchester-dev
      - shapwick-dev
      - tinwell-dev
  {{ end }}

  - action: overlay
    source: overlays/hostname

  - action: overlay
    source: overlays/default-hosts

  - action: overlay
    source: overlays/loopback-interface

  - action: overlay
    source: overlays/default-locale-c-utf8

  - action: run
    chroot: true
    script: scripts/add-xdg-user-metadata.sh

  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    chroot: true
    description: "Creating mount point /Applications"
    command: install -d -m 0755 /Applications

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    chroot: true
    script: scripts/generate_openssh_keys.sh

  - action: run
    chroot: true
    script: scripts/add-initramfs-modules.sh

  - action: run
    description: "Generate locales"
    chroot: true
    script: scripts/generate_locales.sh

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $ospack }}.pkglist.gz"

  - action: run
    description: List files on {{ $ospack }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $ospack }}.filelist.gz"

  - action: pack
    compression: gz
    file: {{ $ospack }}.tar.gz
