{{- $architecture := or .architecture "amd64" }}
{{- $type := or .type "minimal" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "18.12" -}}
{{- $timestamp := or .timestamp "00000000.0" -}}
{{- $ospack := or .ospack (printf "ospack_%s-%s-%s_%s" $suite $architecture $type $timestamp) -}}
{{- $lxc := or .lxc "enabled" -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{ $suite }}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: apertis-archive-keyring
    keyring-file: keyring/apertis-archive-keyring.gpg
    merged-usr: false

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh apertis {{ $suite }} {{ $timestamp }} collabora {{ $type }}

  # Extend apt sources list
  - action: run
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} target

  - action: apt
    description: "Core packages"
    packages:
      - sudo
      - apt-transport-https
      - apertis-customizations
      - initramfs-tools
      - udisks2

  - action: apt
    description: "Base packages"
    packages:
      - busybox
      - busybox-initramfs
      - dbus-user-session

  - action: apt
    description: "Networking packages"
    packages:
      - connman
      - wpasupplicant

  - action: apt
    description: "AppArmor packages"
    packages:
      - apparmor
      - chaiwala-apparmor-profiles

  - action: apt
    description: "Application framework packages"
    packages:
      - canterbury-core
      - newport
      - ribchester-core

  - action: apt
    description: "Test environment packages"
    packages:
      - net-tools
      - openssh-client
      - openssh-server
      - vim.tiny

  - action: overlay
    source: overlays/hostname

  - action: overlay
    source: overlays/default-hosts

  - action: overlay
    source: overlays/loopback-interface

  - action: overlay
    source: overlays/default-locale-c-utf8

{{ if eq $architecture "amd64" }}
  - action: overlay
    source: overlays/initramfs-modules-amd64
{{ end }}

{{ if eq $architecture "armhf" }}
  - action: overlay
    source: overlays/initramfs-modules-imx6
{{ end }}

  - action: overlay
    source: overlays/minimal-dpkg-exclusions

  - action: overlay
    source: overlays/minimal-ribchester-polkit-rules

  - action: overlay
    source: overlays/minimal-automount-rules

  - action: run
    chroot: true
    script: scripts/add-xdg-user-metadata.sh

  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    chroot: true
    description: "Creating mount point /Applications"
    command: install -d -m 0755 /Applications

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    chroot: true
    script: scripts/generate_openssh_keys.sh

  - action: run
    chroot: true
    script: scripts/add-initramfs-modules.sh

  - action: run
    description: "Generate locales"
    chroot: true
    script: scripts/generate_locales.sh

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $ospack }}.pkglist.gz"

  - action: run
    description: List files on {{ $ospack }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $ospack }}.filelist.gz"

  - action: pack
    compression: gz
    file: {{ $ospack }}.tar.gz
