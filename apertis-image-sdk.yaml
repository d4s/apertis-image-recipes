{{ $architecture := or .architecture "amd64" }}
{{ $type := or .type "sdk" }}
{{ $mirror := or .mirror "https://repositories.apertis.org/apertis/" }}
{{ $suite := or .suite "18.12" }}
{{ $timestamp := or .timestamp "00000000.0" }}
{{ $imageroot := or .imageroot (printf "https://images.apertis.org/daily/%s/%s" $suite $timestamp) }}
{{ $ospack := or .ospack (printf "ospack_%s-%s-%s_%s" $suite $architecture $type $timestamp) }}
{{ $image := or .image (printf "apertis-%s-%s-%s_%s" $suite  $type $architecture $timestamp) }}

{{ $cmdline := or .cmdline "console=tty0 console=ttyS0,115200n8 rootwait ro quiet splash plymouth.ignore-serial-consoles" }}

{{ $demopack := or .demopack "disabled" }}

{{ $size := or .size "20G" }}
{{ $sampleappscheckout := or .sampleappscheckout "disabled" }}
{{ $devroot := or .devroot "enabled" }}
{{ $devroot_arch := or .devroot_arch "armhf" }}
{{ $devpack := or .devpack (printf "ospack_%s-%s-%s_%s" $suite $devroot_arch "devroot" $timestamp) }}

architecture: {{ $architecture }}

actions:

  - action: unpack
    description: Unpack {{ $ospack }}
    compression: gz
    file: {{ $ospack }}.tar.gz

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
  {{ if ne $demopack "disabled" }}
  - action: download
    url: {{ $demopack }}
    name: mediademo

  - action: unpack
    origin: mediademo
  {{ end }}

  - action: image-partition
    imagename: {{ $image }}.img
    imagesize: {{ $size }}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
        flags: [ boot ]
      - mountpoint: /boot/efi
        partition: EFI
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: EFI
        fs: vfat
        start: 6176s
        end: 256M
        flags: [ boot ]
      - name: system
        fs: btrfs
        start: 256M
        end: 10G
      - name: general_storage
        fs: btrfs
        start: 10G
        end: 100%

  - action: filesystem-deploy
    description: Deploying ospack onto image
    append-kernel-cmdline: {{ $cmdline }}

  - action: apt
    packages:
      - systemd-boot

  - action: run
    description: Install UEFI bootloader
    chroot: true
    command: bootctl --path=/boot/efi install

  - action: apt
    description: Kernel and system packages for {{$architecture}}
    packages:
      - linux-image-{{$architecture}}
      - btrfs-tools
      - libgles2-mesa

  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $image }}.pkglist.gz"

  - action: run
    description: Cleanup /var/lib
    script: scripts/remove_var_lib_parts.sh

  {{ if eq $sampleappscheckout "enabled" }}
  - action: run
    description: Fetch sample app bundle sources
    chroot: true
    script: scripts/clone-sample-repos.sh
  {{ end }}

  {{ if eq $devroot "enabled" }}
  - action: download
    description: Fetch the devroot ospack
    url: {{ $imageroot }}/{{ $timestamp }}/{{ $devroot_arch }}/devroot/{{ $devpack }}.tar.gz
    name: devpack
    filename: devpack.tar.gz

  - action: run
    description: Create the devroot directory
    chroot: false
    command: mkdir -p "${ROOTDIR}/opt/devroot"

  - action: run
    description: Unpack the devroot ospack
    chroot: false
    command: tar --extract -C "${ROOTDIR}/opt/devroot" -f devpack.tar.gz
  {{ end }}

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.filelist.gz"

  # For VirtualBox
  - action: run
    description: Convert raw image to {{ $image }}.vdi
    postprocess: true
    command: qemu-img convert -O vdi {{ $image }}.img {{ $image }}.vdi

  - action: run
    description: Compress {{ $image }}.vdi
    postprocess: true
    command: gzip -f {{ $image }}.vdi

  - action: run
    description: Checksum for {{ $image }}.vdi.gz
    postprocess: true
    command: sha256sum {{ $image }}.vdi.gz > {{ $image }}.vdi.gz.sha256

  # Raw image
  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum {{ $image }}.img.gz > {{ $image }}.img.gz.sha256
